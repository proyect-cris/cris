<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Researchers</title>
<%@ include file = "Header.jsp"%>
</head>

<body>
	
	<c:if test="${ri.id == user.id}"> 
	<h2>Update publications of researcher</h2> 
	<a href="UpdateCitationsAPIServlet?id=${pi.id}"> ${pi.id}</a>
	</c:if>
	<table>  
	<tr>   
	 <th>Id</th><th>Title</th><th>Publication name</th><th>Date</th><th>Authors</th><th>Cite count</th>  
	</tr>  
	 <tr><td>${pi.id}</td>      
	 <td>${pi.title}</td>     
	  <td>${pi.publicationName}</td>      
	  <td>${pi.publicationDate}</td>      
	  <td>${pi.authors}</td>      
	  <td>${pi.citeCount}</td>  
	 </tr>
	</table>
 </body>
</html>