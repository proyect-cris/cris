package es.upm.dit.apsv.cris.servlets;



import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Researcher;




@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request, response);

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		Researcher researcher = (Researcher) request.getSession().getAttribute("user");
		
		if (researcher != null && researcher.getId().contentEquals("root")) {
			 String id = request.getParameter("id");
			 String name = request.getParameter("name");
			 String lastname = request.getParameter("lastname");
			 String email = request.getParameter("email");
			 String password = request.getParameter("password");
			 
			 Researcher rnew =  new Researcher();
			 rnew.setId(id);
			 rnew.setName(name);
			 rnew.setLastname(lastname);
			 rnew.setEmail(email);
			 rnew.setPassword(password);
			 
			 Client client = ClientBuilder.newClient(new ClientConfig());
			 try {
			        
			        client.target( URLHelper.getInstance().getCrisURL() + "/rest/Researchers/"+ id).request()
			                .accept(MediaType.APPLICATION_JSON).get(Researcher.class);
			        return;
			       
			    } catch(Exception e) {}
			 
			client.target( URLHelper.getInstance().getCrisURL() + "/rest/Researchers/").request()
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(rnew, MediaType.APPLICATION_JSON), Response.class);
			response.sendRedirect(request.getContextPath() + "/AdminServlet");
			
		}else {
		      request.getSession().invalidate();
		      request.setAttribute("message", "Invalid user or password");
		      getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		    }  
			
	}

}
